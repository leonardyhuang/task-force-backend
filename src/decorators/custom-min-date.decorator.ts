import { buildMessage, ValidateBy, ValidationOptions } from 'class-validator';

export const MIN_DATE = 'minDate';

/**
 * Checks if the value is a date that's after the specified date.
 */
export function CustomMinDate(
  minDate: Date,
  validationOptions?: ValidationOptions,
): PropertyDecorator {
  return ValidateBy(
    {
      name: MIN_DATE,
      constraints: [minDate],
      validator: {
        validate: (value: any): boolean => customMinDate(value, minDate),
        defaultMessage: buildMessage(
          (eachPrefix) =>
            'minimum allowed date for ' +
            eachPrefix +
            `$property is ${minDate.toISOString().split('T')[0]}`,
          validationOptions,
        ),
      },
    },
    validationOptions,
  );
}

/**
 * Checks if the value is a date that's after the specified date.
 */
export function customMinDate(date: unknown, minDate: Date): boolean {
  const dateObject: Date =
    typeof date === 'string' ? new Date(date) : <Date>date;

  return (
    dateObject instanceof Date &&
    dateObject.toISOString().split('T')[0] >=
      minDate.toISOString().split('T')[0]
  );
}
