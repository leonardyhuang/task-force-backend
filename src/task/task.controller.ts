import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TaskService } from './task.service';

@Controller('tasks')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async create(@Body() createTaskDto: CreateTaskDto) {
    return await this.taskService.create(createTaskDto);
  }

  @Get()
  async find(
    @Query('name_like') nameLike: string,
    @Query('_sort') sort: string,
    @Query('_order') order: string,
    @Query('_start') start: number,
    @Query('_end') end: number,
    @Res() res: any,
  ) {
    const results = await this.taskService.findAndCount(
      nameLike,
      sort,
      order,
      start,
      end,
    );

    res
      .header('access-control-expose-headers', 'X-Total-Count')
      .header('x-total-count', results[1])
      .send(results[0]);
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.taskService.findOne(+id);
  }

  @Patch(':id')
  @UsePipes(ValidationPipe)
  async update(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto) {
    return await this.taskService.update(+id, updateTaskDto);
  }

  @Put(':id/mark-complete')
  async markComplete(@Param('id') id: string) {
    return await this.taskService.markComplete(+id);
  }

  @Put(':id/mark-incomplete')
  async markIncomplete(@Param('id') id: string) {
    return await this.taskService.markIncomplete(+id);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.taskService.remove(+id);
  }
}
