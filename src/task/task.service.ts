import { Injectable } from '@nestjs/common';
import { TaskEntity } from 'src/entities';
import { FindManyOptions, ILike } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TaskService {
  async create(createTaskDto: CreateTaskDto) {
    createTaskDto.dueDate = new Date(
      new Date(createTaskDto.dueDate).getTime() +
        Number(process.env.APP_HOURS_DIFFERENCE_WITH_UTC) * 60 * 60 * 1000,
    )
      .toISOString()
      .split('T')[0];

    return await TaskEntity.save({ ...createTaskDto });
  }

  async findAndCount(
    nameLike: string,
    sort: string,
    order: string,
    start: number,
    end: number,
  ) {
    const options: FindManyOptions = {
      where: { name: ILike(`${nameLike ?? ''}%`) },
      order: {
        [sort ?? 'id']: order ? (order === 'desc' ? 'desc' : 'asc') : 'asc',
      },
      skip: start,
      take: end - start,
    };

    return await TaskEntity.findAndCount(options);
  }

  async findOne(id: number) {
    return await TaskEntity.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateTaskDto: UpdateTaskDto) {
    if (updateTaskDto.dueDate) {
      updateTaskDto.dueDate = new Date(
        new Date(updateTaskDto.dueDate).getTime() +
          Number(process.env.APP_HOURS_DIFFERENCE_WITH_UTC) * 60 * 60 * 1000,
      )
        .toISOString()
        .split('T')[0];
    }

    return await TaskEntity.createQueryBuilder()
      .update(TaskEntity)
      .set({ ...updateTaskDto })
      .where('id = :id returning *', { id: id })
      .execute()
      .then((response) => response.raw[0]);
  }

  async markComplete(id: number) {
    return await TaskEntity.createQueryBuilder()
      .update(TaskEntity)
      .set({
        completed: true,
        completedAt: new Date(),
      })
      .where('id = :id returning *', { id: id })
      .execute()
      .then((response) => response.raw[0]);
  }

  async markIncomplete(id: number) {
    return await TaskEntity.createQueryBuilder()
      .update(TaskEntity)
      .set({
        completed: false,
        completedAt: null,
      })
      .where('id = :id returning *', { id: id })
      .execute()
      .then((response) => response.raw[0]);
  }

  async remove(id: number) {
    return await TaskEntity.delete(id);
  }
}
