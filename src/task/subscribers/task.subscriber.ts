import { EntitySubscriberInterface, EventSubscriber } from 'typeorm';
import { TaskEntity } from '../entities/task.entity';

@EventSubscriber()
export class TaskSubscriber implements EntitySubscriberInterface<TaskEntity> {
  listenTo() {
    return TaskEntity;
  }

  async afterLoad(task: TaskEntity): Promise<void> {
    const today = new Date(
      new Date(
        new Date().getTime() +
          Number(process.env.APP_HOURS_DIFFERENCE_WITH_UTC) * 60 * 60 * 1000,
      )
        .toISOString()
        .split('T')[0],
    );
    const dueDate = new Date(task.dueDate);
    const dayDiff = (dueDate.getTime() - today.getTime()) / (1000 * 3600 * 24);

    if (dayDiff < 0) {
      task.status = 'Overdue';

      return;
    }

    if (dayDiff >= 0 && dayDiff <= 7) {
      task.status = 'Due soon';

      return;
    }

    task.status = 'Not urgent';
  }
}
