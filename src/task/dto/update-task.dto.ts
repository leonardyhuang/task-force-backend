import { PartialType } from '@nestjs/mapped-types';
import { IsDateString, IsOptional, IsString, MaxLength } from 'class-validator';
import { CreateTaskDto } from './create-task.dto';

export class UpdateTaskDto extends PartialType(CreateTaskDto) {
  @IsOptional()
  @IsString()
  @MaxLength(191)
  name: string;

  @IsOptional()
  @IsString()
  @MaxLength(191)
  description: string;

  @IsOptional()
  @IsDateString()
  dueDate: string;
}
