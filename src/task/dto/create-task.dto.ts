import { IsDateString, IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateTaskDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(191)
  name: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(191)
  description: string;

  @IsNotEmpty()
  @IsDateString()
  dueDate: string;
}
