import { TaskEntity } from './task/entities/task.entity';

const entities = [TaskEntity];

export { TaskEntity };
export default entities;
